<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * @OA\Get(
 *     path="/api/ping",
 *     tags={"ping"},
 *     summary="ping",
 *     description="ping description",
 *     operationId="ping",
 *     @OA\Parameter(
 *         name="status",
 *         in="query",
 *         description="Status values that needed to be considered for filter",
 *         required=true,
 *         explode=true,
 *         @OA\Schema(
 *             default="available",
 *             type="string",
 *             enum={"available", "pending", "sold"},
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="successful operation",
 *
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Invalid status value"
 *     ),
 * )
 */
class PingController extends Controller {

    public function ping()
    {
        return 'Hello';
    }
}
