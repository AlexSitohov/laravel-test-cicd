<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Requests\ProductStoreRequest;
use Illuminate\Support\Str;

class ProductController extends Controller {

    public function index()
    {
        $products = Product::all();
        return response($products);
    }

    public function store(ProductStoreRequest $request)
    {
        $data = $request->validated();
        $data['id'] = Str::uuid()->toString();
        $product = Product::make($data);
        $product->save();
        return response($product);

    }

    public function show(Product $product) {
        return response($product);
    }
}
