<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UuidTrait;

class Product extends Model {

    use HasFactory;
    use UuidTrait;

    protected $guarded = [];
}
